#ifndef CUSTOMEXCEPTION_HPP
# define CUSTOMEXCEPTION_HPP

#include <string>

class CustomException: public std::exception
{
public:
	CustomException(void);
	CustomException(const std::string message);
	CustomException(CustomException const &cpy);
	virtual ~CustomException() _NOEXCEPT;

	const std::string getMessage() const;
	void setMessage(const std::string cpy);

	CustomException & operator=(CustomException const &rhs);

	virtual const char* what() const throw();

private:
	std::string message;
};

std::ostream &operator<<(std::ostream &outStream, const CustomException &toWrite);

#endif