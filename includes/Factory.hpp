#ifndef FACTORY_HPP
# define FACTORY_HPP

#include <string>
#include <array>
#include "enum.hpp"
#include "IOperand.hpp"

class Factory;

typedef IOperand const * (Factory::* ioPf)( std::string const & value ) const;

class Factory {

	public:
		static Factory *getInstance(void);
		static void	kill(void);
		IOperand const * createOperand( eOperandType type, std::string const & value ) const;

		int 	test(void);

	private:
		Factory();
		Factory(Factory const &cpy);
		~Factory();

		std::array<ioPf, 6> 		peuplateOperandFct() const;

		Factory & operator=(Factory const &rhs);

		IOperand const * createNone( std::string const & value ) const;
		IOperand const * createInt8( std::string const & value ) const;
		IOperand const * createInt16( std::string const & value ) const;
		IOperand const * createInt32( std::string const & value ) const;
		IOperand const * createFloat( std::string const & value ) const;
		IOperand const * createDouble( std::string const & value ) const;

		static Factory			*instance;

};


#endif