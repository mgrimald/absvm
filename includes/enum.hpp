#ifndef ENUM_HPP
# define ENUM_HPP

typedef enum operandType{ 
	None,
	Int8,
	Int16,
	Int32,
	Float,
	Double
} eOperandType;

typedef enum lexedTextPosition{ 
	INSTRUCTION,
	OPERAND,
	VALUE
} eLexedTextPosition;

typedef enum instructionType {
	NONE,
	PUSH,
	POP,
	DUMP,
	ASSERT,
	ADD,
	SUB,
	MUL,
	DIV,
	MOD,
	PRINT,
	EXIT
}	eInstructionType;

#endif