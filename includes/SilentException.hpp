#ifndef SILENTEXCEPTION_HPP
# define SILENTEXCEPTION_HPP

#include "CustomException.hpp"

class SilentException: public CustomException
{
public:
	SilentException(void);
	SilentException(SilentException const &cpy);
	virtual ~SilentException() _NOEXCEPT;

	SilentException & operator=(SilentException const &rhs);
	
	virtual const char* what() const throw();
};

std::ostream &operator<<(std::ostream &outStream, const SilentException &toWrite);
#endif