#ifndef ABSTRACTVM_HPP
# define ABSTRACTVM_HPP

# include <iostream>
# include <istream>
# include <fstream>
# include <queue>
# include <deque>

# include "Instruction.hpp"
# include "SilentException.hpp"
# include "CustomException.hpp"
# include "IOperand.hpp"

class AbstractVm;

typedef std::deque<const IOperand*> StaQ;

typedef int (AbstractVm::* avmPf)(Instruction const &inst);

class AbstractVm {
	public:
		AbstractVm(void);
		AbstractVm(AbstractVm const &cpy);
		virtual ~AbstractVm();
		AbstractVm & operator=(AbstractVm const &rhs);

		const std::queue<Instruction>	getInstructions(void) const;								//not tested
		void							setInstructions(std::queue<Instruction> const &cpy);		//not tested
				
		const StaQ		getStackData(void) const;									//not tested
		void							setStackData(StaQ const &cpy);				//not tested

		std::queue<Instruction>			readInputs(std::string fileToRead);

		int  push	(Instruction const &inst);
		int  pop	(Instruction const &inst);
		int  dump	(Instruction const &inst);
		int  assert	(Instruction const &inst);
		int  add	(Instruction const &inst);
		int  sub	(Instruction const &inst);
		int  mul	(Instruction const &inst);
		int  div	(Instruction const &inst);
		int  mod	(Instruction const &inst);
		int  print	(Instruction const &inst);
		int  exit	(Instruction const &inst);
		int  none	(Instruction const &inst);

		int	run(void);
		int	run(std::string file);
		int exec(void);


		int test(void);

		
	private:
		std::queue<Instruction>		instructions;
		StaQ						stackData;
		std::queue<Instruction>		readInputs(std::istream &inStream, bool isCin = false);

};

std::ostream &operator<<(std::ostream &outStream, const AbstractVm &toWrite);


#endif