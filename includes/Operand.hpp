#ifndef OPERAND_HPP
# define OPERAND_HPP

#include <iostream>
#include <limits>
#include <sstream>
#include <math.h>				//fmod
#include "IOperand.hpp"
#include "Factory.hpp"
#include "CustomException.hpp"

template <typename T>
class Operand: public IOperand {
	public:
		Operand(eOperandType type, std::string const & value ) {
			double v;

			this->s = value;
			this->type = type;
			try {
				v = std::stod(value);
			}
			catch (std::exception){
				throw( CustomException("[CALCULUS][CONVERSION] string value (" + value + ") cannot be transformed to a numeric value"));
			}
			if (v < std::numeric_limits<T>::lowest())
				throw  CustomException("[CALCULUS]: min numeric_limit for this type is " + std::to_string(std::numeric_limits<T>::min()) + ". value given is " + value);
			if (v > std::numeric_limits<T>::max())
				throw CustomException("[CALCULUS]: max numeric_limit for this type is " + std::to_string(std::numeric_limits<T>::max()) + ". value given is " + value);
			this->value = static_cast<T>(v);

			this->s = std::to_string(this->value);
			if (this->s.find('.') !=std::string::npos){
				this->s.erase(this->s.find_last_not_of('0') + 1, std::string::npos);
				if (this->s.back() == '.'){
					this->s.pop_back();
				}
			}
		}

		virtual int getPrecision( void ) const {
			return (this->type);
		}

		virtual eOperandType getType( void ) const {
			return (this->type);
		}

		virtual IOperand const * operator%( IOperand const & rhs ) const {
			int precision = this->getPrecision();
			if (precision < rhs.getPrecision())
				precision = rhs.getPrecision();
			double rvalue = 0;
			try {
				rvalue = std::stod(rhs.toString());
			}
			catch (std::exception){
				throw(CustomException("[CALCULUS][modulo][CONVERSION] string value (" + rhs.toString() + ") cannot be transformed to a numeric value"));
			}
			if (!rvalue)							// for division and modulo
				throw CustomException("[CALCULUS]: modulo (therefore division) by zero");
			std::stringstream ret; 
			ret << fmod(this->value, rvalue); // Modulo
			return (Factory::getInstance()->createOperand(static_cast<eOperandType>(precision), ret.str()));
		}

		virtual IOperand const * operator/( IOperand const & rhs ) const {
			int precision = this->getPrecision();
			if (precision < rhs.getPrecision())
				precision = rhs.getPrecision();
			double rvalue = 0;
			try {
				rvalue = std::stod(rhs.toString());
			}
			catch (std::exception){
				throw(CustomException("[CALCULUS][division][CONVERSION] string value (" + rhs.toString() + ") cannot be transformed to a numeric value"));
			}
			if (!rvalue)							// for division and modulo
				throw CustomException("[CALCULUS]: division by zero");
			std::stringstream ret; 
			ret << this->value / rvalue;      // division
			return (Factory::getInstance()->createOperand(static_cast<eOperandType>(precision), ret.str()));
		}

		virtual IOperand const * operator+( IOperand const & rhs ) const {
			int precision = this->getPrecision();
			if (precision < rhs.getPrecision())
				precision = rhs.getPrecision();
			double rvalue = 0;
			try {
				rvalue = std::stod(rhs.toString());
			}
			catch (std::exception){
				throw(CustomException("[CALCULUS][addition][CONVERSION] string value (" + rhs.toString() + ") cannot be transformed to a numeric value"));
			}
			std::stringstream ret; 
			ret << this->value + rvalue;      // addition
			return (Factory::getInstance()->createOperand(static_cast<eOperandType>(precision), ret.str()));
		}		

		virtual IOperand const * operator-( IOperand const & rhs ) const {
			int precision = this->getPrecision();
			if (precision < rhs.getPrecision())
				precision = rhs.getPrecision();
			double rvalue = 0;
			try {
				rvalue = std::stod(rhs.toString());
			}
			catch (std::exception){
				throw(CustomException("[CALCULUS][substraction][CONVERSION] string value (" + rhs.toString() + ") cannot be transformed to a numeric value"));
			}
			std::stringstream ret; 
			ret << this->value - rvalue;      // substraction
			return (Factory::getInstance()->createOperand(static_cast<eOperandType>(precision), ret.str()));
		}		

		virtual IOperand const * operator*( IOperand const & rhs ) const {
			int precision = this->getPrecision();
			if (precision < rhs.getPrecision())
				precision = rhs.getPrecision();
			double rvalue = 0;
			try {
				rvalue = std::stod(rhs.toString());
			}
			catch (std::exception){
				throw(CustomException("[CALCULUS][multiplication][CONVERSION] string value (" + rhs.toString() + ") cannot be transformed to a numeric value"));
			}
			std::stringstream ret;
			ret << this->value * rvalue;      // multiplication
			return (Factory::getInstance()->createOperand(static_cast<eOperandType>(precision), ret.str()));
		}		

		virtual std::string const & toString( void ) const {
			return (this->s);
		}

		virtual ~Operand( void ) {}

	private:
		Operand( void );
		Operand( Operand const & src );
		Operand	& operator=( Operand const & rhs );
		eOperandType	type;
		T				value;
		std::string		s;
};


#endif
