#ifndef INSTRUCTION_HPP
# define INSTRUCTION_HPP

#include <string>
#include <array>

# include "enum.hpp"


class Instruction {
public:
	Instruction(void);
	Instruction(const std::string txt);
	Instruction(Instruction const &cpy);
	virtual ~Instruction();

	const std::string 				 getText() const;
	const std::array<std::string, 3> getLexedText() const;
	const std::string 				 getLexedText(int pos) const;
		  eInstructionType 			 getInstruction(void) const ;
		  eOperandType 				 getOperand(void) const ;

	void setText 		(const std::string cpy);
	void setLexedText	(const std::array<std::string, 3> cpy);
	void setLexedText	(const std::string cpy, int pos);
	void setInstruction	(const eInstructionType cpy);
	void setOperand		(const eOperandType cpy); 

	void	lexe(void);
	void	parse(void);

	void				parseOperandAndValue(void);
	void				parseInstruction(void);

	int test(void);
	
	Instruction & operator=(Instruction const &rhs);

private:
	eOperandType		parseOperand(std::string str) const;
	eInstructionType	parseInstruction(std::string str) const;

	std::string 				text;
	std::array<std::string, 3> 	lexedText;
	eInstructionType			instruction;
	eOperandType				operand;
};

std::ostream &operator<<(std::ostream &outStream, const Instruction &toWrite);

#endif