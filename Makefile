NAME = avm

HDIR = ./includes/
SDIR = ./sources/
ODIR = objects/

SRC = main.cpp \
	  AbstractVm.cpp \
	  CustomException.cpp \
	  SilentException.cpp \
	  Instruction.cpp \
	  tests.cpp \
	  Factory.cpp

OBJ = $(SRC:.cpp=.o)

SRC_C := $(addprefix $(SDIR), $(SRC))
SRC_O := $(addprefix $(ODIR), $(OBJ))

CC = g++

FLAGS = -g -Wall -Wextra -Werror --std=c++11

RED = \033[31m
CYAN = \033[36m
NORMAL = \033[00m
GREY = \033[90m

.PHONY: clean fclean all re propre

all: $(NAME)

$(NAME): $(SRC_O)
	@$(CC) $(FLAGS) -o $(NAME) $(SRC_O)
	@echo "$(CYAN)[$(NAME)] $(RED)[$(NAME)]$(GREY) \t\t\tExecutable created.$(NORMAL)"

$(ODIR)%.o: $(SDIR)/%.cpp
	@mkdir -p $(ODIR);
	@$(CC) $(FLAGS) -c $< -o $@ -I $(HDIR)
	@echo "$(CYAN)[$(NAME)] $(RED)[$@]$(GREY) \tObjects created.$(NORMAL)"

re: fclean all

clean:
	@rm -rf $(ODIR)
	@echo "$(CYAN)[$(NAME)]$(RED) [$(ODIR)]$(GREY) \t\t\tAll objects file deleted.$(NORMAL)"

fclean: clean
	@rm -f $(NAME)
	@echo "$(CYAN)[$(NAME)]$(RED) [$(NAME)]$(GREY) \t\t\tExecutable deleted.$(NORMAL)"

propre: all clean
