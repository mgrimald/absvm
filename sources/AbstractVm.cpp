#define INSTRUCTION_VERBOSE 		 false
						//default to false

/*    it's easier to test from cin  if you comment this block and uncomment the next one     */
#define CIN_EXECUTE_DIRECTLY 		 false
						//default to false
#define STOP_CIN_FIRST_PARSING_ERROR true
						//default to true
#define STOP_CIN_FIRST_RUNTIME_ERROR true
						//default to true
/**/




/*    easier to test from cin       */
/*    
#define CIN_EXECUTE_DIRECTLY 		 true
						//default to false

#define STOP_CIN_FIRST_PARSING_ERROR false
						//default to true
#define STOP_CIN_FIRST_RUNTIME_ERROR false
						//default to true
*/     


#include <regex>
#include "AbstractVm.hpp"
#include "Operand.hpp"
#include "Factory.hpp"


static std::array<avmPf, 12> peuplateInstructionFct(){
	std::array<avmPf, 12> instructionsFct;

	instructionsFct[NONE] 	= &AbstractVm::none;
	instructionsFct[PUSH] 	= &AbstractVm::push;
	instructionsFct[POP] 	= &AbstractVm::pop;
	instructionsFct[DUMP] 	= &AbstractVm::dump;
	instructionsFct[ASSERT] = &AbstractVm::assert;
	instructionsFct[ADD] 	= &AbstractVm::add;
	instructionsFct[SUB] 	= &AbstractVm::sub;
	instructionsFct[MUL] 	= &AbstractVm::mul;
	instructionsFct[DIV] 	= &AbstractVm::div;
	instructionsFct[MOD] 	= &AbstractVm::mod;
	instructionsFct[PRINT] 	= &AbstractVm::print;
	instructionsFct[EXIT] 	= &AbstractVm::exit;

	return instructionsFct;
}

const std::queue<Instruction>	AbstractVm::getInstructions(void) const {return instructions;}
void							AbstractVm::setInstructions(std::queue<Instruction> const &cpy){instructions = cpy;}
const StaQ						AbstractVm::getStackData(void) const{	return stackData; }
void							AbstractVm::setStackData(StaQ  const &cpy){	stackData = cpy;} 

AbstractVm::AbstractVm(AbstractVm const &cpy){
	*this = cpy;
}

AbstractVm::AbstractVm(void){
}

AbstractVm::~AbstractVm(){}

AbstractVm & AbstractVm::operator=(AbstractVm const &rhs){
	instructions = rhs.getInstructions();
	stackData = rhs.getStackData();
	return *this;
}

std::ostream &operator<<(std::ostream &outStream, const AbstractVm &toWrite){
	outStream << "[<<AbstractVm]\t" << "address:\t" << &toWrite << std::endl;
	return outStream;
}



std::queue<Instruction>	AbstractVm::readInputs(std::istream &inStream, bool isCin){
	std::array<avmPf, 12> instructionsFct = peuplateInstructionFct();
	std::regex const pattern_comment(R"(^\s*;.*$)");
	std::regex const pattern_empty(R"(^\s*$)");
	bool exiit = false;

	std::string 			str;
	std::queue<Instruction> q;
	std::string 			completeErrorMessage;

	for (int i = 1; getline(inStream, str); i++){
		if (isCin == true && str.compare(";;") == 0){
			break ;
		}
		if (std::regex_match(str, pattern_empty) || std::regex_match(str, pattern_comment)){
			continue ;
		}
		Instruction inst(str);
		try {
			inst.lexe();
			inst.parse();
		}
		catch (const CustomException& e){
			std::string ret = e.what();
			ret += "\n\t[parsing][l:" + std::to_string(i) + "]: " + inst.getText();
			if (inst.getLexedText(OPERAND).empty() && inst.getLexedText(VALUE).empty())
				ret += "\n\ttokenized as |=|" + inst.getLexedText(INSTRUCTION) + "|=|";
			else
				ret += "\n\ttokenized as |=|" + inst.getLexedText(INSTRUCTION) + "|=|" + inst.getLexedText(OPERAND) + "|=|" + inst.getLexedText(VALUE) + "|=|"; 
			if (!STOP_CIN_FIRST_PARSING_ERROR && isCin)
				std::cout << ret << std::endl;
			else
				completeErrorMessage += ret + "\n\n";
			if (isCin){
				if (STOP_CIN_FIRST_PARSING_ERROR)
					break;
				else
					continue;
			}
		}
		if (CIN_EXECUTE_DIRECTLY && isCin){
			try {
				if ((this->*(instructionsFct[inst.getInstruction()]))(inst) == 1){ 
					exiit = true;
					break;
				}
			}
			catch (const CustomException& e){
				std::string s = e.what();
				s = "[ERROR][RUNTIME]: " + s;
				if (STOP_CIN_FIRST_RUNTIME_ERROR)
					throw (CustomException(s));
				else
					std::cout << s << std::endl;
			}
		}
		else
			q.push(inst);
	}
	if (exiit == false && isCin && CIN_EXECUTE_DIRECTLY && completeErrorMessage.empty() == true){
		throw (CustomException("[ERROR] No Exit instruction given"));
	}
	if (completeErrorMessage.empty() == false){
		throw (CustomException(completeErrorMessage));
	}
	return q;
}

std::queue<Instruction>	AbstractVm::readInputs(std::string fileToRead){
 	std::ifstream	myfile;

	myfile.open(fileToRead);
	if (!myfile.is_open()) {
		throw (CustomException("File '" + fileToRead + "' could not be opened"));
	}
	try {
		return readInputs(myfile);
	}
	catch (const CustomException& e){
		std::string ret = "";
		ret += "FROM FILE '" + fileToRead + "'\n";
		ret += e.what();
		throw (CustomException(ret));
	}
}


int AbstractVm::push	(Instruction const &inst){
	try {
		if (INSTRUCTION_VERBOSE)
			std::cout << "[push]" << std::endl;
		stackData.push_front(Factory::getInstance()->createOperand(inst.getOperand(), inst.getLexedText(VALUE)));
	}
	catch (const CustomException & e) {
		std::string s = e.what();
		throw (CustomException("[push]" + s));
	}
	return 0;
}
int AbstractVm::pop	(Instruction const &inst){
	(void)inst;
	if (INSTRUCTION_VERBOSE)
		std::cout << "[pop]" << std::endl;
	if (stackData.begin() != stackData.end()){
		IOperand const *toDelete = stackData.front();
		delete(toDelete);
		stackData.pop_front();
	}
	else
		throw (CustomException("[pop]: Pop instruction called but the stack is empty."));
	return 0;
}
int AbstractVm::dump	(Instruction const &inst){
	(void)inst;
	int i = 0;
	if (INSTRUCTION_VERBOSE)
		std::cout << "[dump]\t\n";

	if (stackData.begin() != stackData.end()){
		for (StaQ::iterator it = stackData.begin(); it != stackData.end(); it++){
			i++;
			if (INSTRUCTION_VERBOSE){
				std::cout << "\t(" << i  << ")\t"; 
				std::cout << "[" << (*it)->getType() << "]\t";
			}
			std::cout << (*it)->toString() << std::endl;
		}
	}
	else {
		std::cout << "empty stack" << std::endl;
	}
	return 0;
}

int AbstractVm::assert	(Instruction const &inst){
	if (INSTRUCTION_VERBOSE)
		std::cout << "[assert]\t" << std::endl;
	if (stackData.begin() == stackData.end())
		throw (CustomException("[assert]: Assert instruction called but the stack is empty."));
	IOperand const *cpr = NULL;
	try {
		cpr = Factory::getInstance()->createOperand(inst.getOperand(), inst.getLexedText(VALUE));
	}
	catch (const CustomException & e) {
		std::string s = e.what();
		throw (CustomException("[assert]" + s));
	}
	if (cpr->getPrecision() != stackData.front()->getPrecision()){
		throw(CustomException("[assert]:Precision of the value given is not equal to the precision of the first element in the stack\n"));		
	}

	std::string c = cpr->toString();
	std::string t = stackData.front()->toString();
	if (c.compare(t) == 0){
		return 0;
	}
	throw(CustomException("[assert]:value given is not equal to the value of the first element in the stack\n\t->" + c + "\n\t->" + t));
	return 0;
}
int AbstractVm::add	(Instruction const &inst){
	(void)inst;

	if (INSTRUCTION_VERBOSE)
		std::cout << "[add]\t"; 
	if (INSTRUCTION_VERBOSE)
		std::cout << std::endl;
	if (stackData.size() < 2){
		throw(CustomException("[add]: not enough element in the stack. We need at least two elements to permforms an addition"));
	}
	IOperand const *top = stackData[0];
	IOperand const *second = stackData[1];
	IOperand const *result = NULL;

	result = *second + *top;
	stackData.pop_front();
	stackData.pop_front();
	delete(top);
	delete(second);
	stackData.push_front(result);
	return 0;
}
int AbstractVm::sub	(Instruction const &inst){
	(void)inst;

	if (INSTRUCTION_VERBOSE)
		std::cout << "[sub]\t"; 
	if (INSTRUCTION_VERBOSE)
		std::cout << std::endl;
	if (stackData.size() < 2){
		throw(CustomException("[sub]: not enough element in the stack. We need at least two elements to permforms a substitution"));
	}
	IOperand const *top = stackData[0];
	IOperand const *second = stackData[1];
	IOperand const *result = NULL;

	result = *second - *top;
	stackData.pop_front();
	stackData.pop_front();
	delete(top);
	delete(second);
	stackData.push_front(result);


	return 0;
}
int AbstractVm::mul	(Instruction const &inst){
	(void)inst;

	if (INSTRUCTION_VERBOSE)
		std::cout << "[mul]\t"; 
	if (INSTRUCTION_VERBOSE)
		std::cout << std::endl;
	if (stackData.size() < 2){
		throw(CustomException("[mul]: not enough element in the stack. We need at least two elements to permforms a multiplication"));
	}
	IOperand const *top = stackData[0];
	IOperand const *second = stackData[1];
	IOperand const *result = NULL;

	result = *second * *top;
	stackData.pop_front();
	stackData.pop_front();
	delete(top);
	delete(second);
	stackData.push_front(result);


	return 0;
}
int AbstractVm::div	(Instruction const &inst){
	(void)inst;

	if (INSTRUCTION_VERBOSE)
		std::cout << "[div]\t"; 
	if (INSTRUCTION_VERBOSE)
		std::cout << std::endl;
	if (stackData.size() < 2){
		throw(CustomException("[div]: not enough element in the stack. We need at least two elements to permforms a division"));
	}
	IOperand const *top = stackData[0];
	IOperand const *second = stackData[1];
	IOperand const *result = NULL;

	result = *second / *top;
	stackData.pop_front();
	stackData.pop_front();
	delete(top);
	delete(second);
	stackData.push_front(result);

	return 0;
}
	
int AbstractVm::mod	(Instruction const &inst){
	(void)inst;
	if (INSTRUCTION_VERBOSE)
		std::cout << "[mod]\t"; 
	if (INSTRUCTION_VERBOSE)
		std::cout << std::endl;
	if (stackData.size() < 2){
		throw(CustomException("[mod]: not enough element in the stack. We need at least two elements to permforms a modulo"));
	}
	IOperand const *top = stackData[0];
	IOperand const *second = stackData[1];
	IOperand const *result = NULL;

	result = *second % *top;
	stackData.pop_front();
	stackData.pop_front();
	delete(top);
	delete(second);
	stackData.push_front(result);
	return 0;
}
int AbstractVm::print	(Instruction const &inst){
	(void)inst;
	if (INSTRUCTION_VERBOSE)
		std::cout << "[print]\t"; 
	if (stackData.begin() == stackData.end())
		throw (CustomException("[print]: Print instruction called but the stack is empty."));
	if (stackData.front()->getType() != Int8)
		throw (CustomException("[print]: Print instruction called but the top element is not an Int8 (equivalence of Char)."));
	char c = std::stoi(stackData.front()->toString());
	std::cout << "" << c << "" << std::endl;
	return 0;
}
int AbstractVm::exit	(Instruction const &inst){
	(void)inst;
	if (INSTRUCTION_VERBOSE)
		std::cout << "[exit]" << std::endl; 
	return 1;
}

int AbstractVm::none	(Instruction const &inst){
	(void)inst;
	if (INSTRUCTION_VERBOSE)
		std::cout << "[none]" << std::endl; 
	//std::cout << inst << "\nnone function called by function ptr" << std::endl; 
	throw(CustomException("this shouldn't happen.\nThis runtime exception should have been catched earlier: during the parsing."));
	return 1;
}

int AbstractVm::run(std::string file){
	try {
		instructions = readInputs(file);
	}
	catch (const CustomException & e) {
		std::cout << "[DATA COLLECTION ERROR]:\n" << e.what() << "\n\n" << "AbstractVm is now aborting" << std::endl;
		return -2;
	}
	return exec();
}

int AbstractVm::exec(void){
	bool exiit = false;
	std::array<avmPf, 12> instructionsFct = peuplateInstructionFct();

	while (!instructions.empty()){
		Instruction inst = instructions.front();
		instructions.pop();
		try {
			if ((this->*(instructionsFct[inst.getInstruction()]))(inst) == 1){ 
				exiit = true; 
				break;	
			}
		}
		catch (const CustomException& e){
			std::string s = e.what();
			s = "[ERROR][RUNTIME]: " + s + "\n";
			throw (CustomException(s));
		}
	}
	if (exiit == false){
		throw (CustomException("[ERROR] No Exit instruction given"));
	}
	return 0;
}

int	AbstractVm::run(void){
	try {
		instructions = readInputs(std::cin, true);
	}
	catch (const CustomException & e) {
		std::cout << "[ERROR]:\n" << e.what() << "\n\n" << "AbstractVm is now aborting" << std::endl;
		return -2;
	}
	if (CIN_EXECUTE_DIRECTLY)
		return 0;
	return exec();
}
