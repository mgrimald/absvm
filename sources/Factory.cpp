#include "Factory.hpp"
#include "Operand.hpp"

Factory* Factory::instance = NULL;

Factory::Factory(Factory const &cpy){
	*this = cpy;
}

Factory *Factory::getInstance(void){
	if (Factory::instance == NULL)
		Factory::instance = new Factory();
	return Factory::instance;
}

void	Factory::kill(void){
	if (Factory::instance)
		delete Factory::instance;
	Factory::instance = NULL;
}

Factory::Factory(void){
}

Factory::~Factory(){}

Factory & Factory::operator=(Factory const &rhs){
	(void)rhs;
	return *this;
}

std::ostream &operator<<(std::ostream &outStream, const Factory &toWrite){
	outStream << "[<<Factory]\t" << "address:\t" << &toWrite << std::endl;
	return outStream;
}

std::ostream &operator<<(std::ostream &outStream, const IOperand &toWrite){
	outStream << "[<<Operand]\n" << "address:\t" << &toWrite << "\ntoString():\t" << toWrite.toString() << std::endl;
	return outStream;
}

std::array<ioPf, 6> Factory::peuplateOperandFct() const {
	std::array<ioPf, 6> op;

	op[None] 	= &Factory::createNone;
	op[Int8] 	= &Factory::createInt8;
	op[Int16] 	= &Factory::createInt16;
	op[Int32] 	= &Factory::createInt32;
	op[Float] 	= &Factory::createFloat;
	op[Double] 	= &Factory::createDouble;

	return op;
}


IOperand const *Factory::createOperand( eOperandType type, std::string const &value ) const {
	static std::array<ioPf, 6> op = peuplateOperandFct();
	return (this->*(op[type]))(value);
}

IOperand const *Factory::createNone( std::string const & value ) const {
	(void)value;
	throw (CustomException("problem of ptr sur fonction. shoudln't happen"));
	return NULL;
}

IOperand const *Factory::createInt8( std::string const & value ) const {
	return new Operand<char>(Int8, value);
}

IOperand const *Factory::createInt16( std::string const & value ) const {
	return new Operand<short int>(Int16, value);
}

IOperand const *Factory::createInt32( std::string const & value ) const {
	return new Operand<int>(Int32, value);
}

IOperand const *Factory::createFloat( std::string const & value ) const {
	return new Operand<float>(Float, value);
}

IOperand const *Factory::createDouble( std::string const & value ) const {
	return new Operand<double>(Double, value);
}

