#include "CustomException.hpp"
#include <iostream>

CustomException::CustomException(CustomException const &cpy){
	*this = cpy;
}

CustomException::CustomException(void) : std::exception(){
	message = "<default message for CustomException>";
}

CustomException::CustomException(const std::string mess) : std::exception(){
	message = mess;
}

CustomException::~CustomException() _NOEXCEPT {

}

const std::string CustomException::getMessage(void) const {
	return message;
}

void	CustomException::setMessage(const std::string cpy){
	message = cpy;
}

CustomException & CustomException::operator=(CustomException const &rhs){
	this->std::exception::operator=(rhs);
	this->setMessage(rhs.getMessage());
	return *this;
}

std::ostream &operator<<(std::ostream &outStream, const CustomException &toWrite){
	outStream << "[<<CustomException]\n" << "address:\t" << &toWrite << "\nwhat():\t" << toWrite.what() << std::endl;
	return outStream;
}

const char* CustomException::what() const throw(){
   	return this->message.c_str();
}