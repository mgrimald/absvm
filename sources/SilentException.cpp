#include "SilentException.hpp"
#include <iostream>

SilentException::SilentException(SilentException const &cpy){
	*this = cpy;
}

SilentException::SilentException(void) : CustomException(""){

}

SilentException::~SilentException() _NOEXCEPT {

}

SilentException & SilentException::operator=(SilentException const &rhs){
	this->CustomException::operator=(rhs);
	return *this;
}

std::ostream &operator<<(std::ostream &outStream, const SilentException &toWrite){
	outStream << "[<<SilentException]\n" << "address:\t" << &toWrite << "\nwhat():\t" << toWrite.what() << std::endl;
	return outStream;
}


const char* SilentException::what() const throw()
{
    return "(chuuuuut, this is a silent exception, used to go back to the upper frame)";
    return "";
}