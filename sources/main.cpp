#include <iostream>
#include "AbstractVm.hpp"
#include "Factory.hpp"

int test(void);

int main(int ac, char **av){
	/*  */
	if (ac > 1 && av[1][0] == '-' && av[1][1] == 't' && av[1][2] == '\0') { 
		return test();
	}
	/*  */
	Factory::getInstance();

	if (ac > 2){
		std::cout << "usage: ./avm [file]" << std::endl;
		return -1;
	}
	try {
		AbstractVm aVm;
		try {
			int r;
			if (ac > 1)
				r = aVm.run(av[1]);
			else
				r = aVm.run();
			Factory::kill();
			return r;
		}
		catch (const SilentException & e) {
			Factory::kill();
			return -1;
		}
		catch (const CustomException & e) {
			std::cout << e.what() << "\n\n" << "AbstractVm is now aborting" << std::endl;
			Factory::kill();
			return -2;
		}
	}
	catch(const std::exception & e){
		std::cout << "unexpected standard exception catch : " << e.what() << std::endl;
	}
	return 0;
}