#include <regex>
#include <iostream>

#include "Instruction.hpp"
#include "CustomException.hpp"

Instruction::Instruction(Instruction const &cpy){
	*this = cpy;
}

Instruction::Instruction(void){
	text = "<Empty Instruction>";
	lexedText[INSTRUCTION] = "";
	lexedText[OPERAND] = "";
	lexedText[VALUE] = "";
	operand = None;
	instruction = NONE;
}

Instruction::Instruction(const std::string mess){
	text = mess;
	lexedText[INSTRUCTION] = "";
	lexedText[OPERAND] = "";
	lexedText[VALUE] = "";
	operand = None;
	instruction = NONE;
}

Instruction::~Instruction(){

}

const std::string Instruction::getText(void) const {
	return text;
}

void	Instruction::setText(const std::string cpy){text = cpy;}
void	Instruction::setLexedText(std::array<std::string, 3> cpy){lexedText = cpy;}
void	Instruction::setLexedText(const std::string cpy, int pos){lexedText[pos] = cpy;}
void 	Instruction::setInstruction(const eInstructionType cpy){instruction = cpy;}
void 	Instruction::setOperand(const eOperandType cpy){operand = cpy;}
const std::string 		Instruction::getLexedText(int pos) const {return lexedText[pos];}
eInstructionType 		Instruction::getInstruction(void) const {return instruction;}
eOperandType 			Instruction::getOperand(void) const {return operand;}
const std::array<std::string, 3> Instruction::getLexedText(void) const {return lexedText;}

Instruction & Instruction::operator=(Instruction const &rhs){
	this->setText(rhs.getText());
	this->setLexedText(rhs.getLexedText());
	this->setOperand(rhs.getOperand());
	this->setInstruction(rhs.getInstruction());
	return *this;
}

std::ostream &operator<<(std::ostream &outStream, const Instruction &toWrite){
	outStream << "[<<Instruction]  " << "address: " << &toWrite << "\n   getText(): '" << toWrite.getText() << "'\n";
	std::array<std::string, 3> lexedText = toWrite.getLexedText();
	outStream << "\tinstruction: (" << toWrite.getInstruction() << ")\tlexedText[INSTRUCTION]: '" << lexedText[INSTRUCTION] << "'" << std::endl;
	outStream << "\toperand    : (" << toWrite.getOperand() << ")\tlexedText[OPERAND]:\t'" << lexedText[OPERAND] << "'" << std::endl;
	outStream << "\t\t\t\tlexedText[VALUE]:\t'" << lexedText[VALUE] << "'" << std::endl;
	return outStream;
}

void	Instruction::lexe(void) {
	std::regex const pattern_1(R"(^\s*(\w+)\s*(;.*)?$)");
	std::regex const pattern_2(R"(^\s*(\w+)\s+([[:alnum:]]+)\s*\(\s*([\S]+)\s*\)\s*(;.*)?$)" );
	std::smatch match;

	std::string to = text;
	lexedText[INSTRUCTION] = text;
	if (std::regex_search(to, match, pattern_1)){
		lexedText[INSTRUCTION] = match[1];
	}
	else if (std::regex_search(to, match, pattern_2)){
		lexedText[INSTRUCTION] = match[1];
		lexedText[OPERAND] = match[2];
		lexedText[VALUE] = match[3];
	}
	else
		throw (CustomException("[lexing] '" + text + "' is not reccognised by the differents regex"));
//	std::cout << *this << std::endl;
}

void	Instruction::parse(void){
	parseInstruction();
	if ((instruction != PUSH && instruction != ASSERT) && !lexedText[OPERAND].empty())
		throw(CustomException("[parsing] only push and assert instructions can be given operand"));
	if (instruction == PUSH && (lexedText[OPERAND].empty() || lexedText[VALUE].empty()))
		throw(CustomException("[parsing] push instruction need to be given an operand and a value"));
	if (instruction == ASSERT && (lexedText[OPERAND].empty() || lexedText[VALUE].empty()))
		throw(CustomException("[parsing] assert instruction need to be given an operand and a value"));
	parseOperandAndValue();
}

void	Instruction::parseOperandAndValue(void){
	if (lexedText[OPERAND].empty() == false){
		operand = parseOperand(lexedText[OPERAND]);

		std::string str = lexedText[VALUE];
		std::regex const pattern_float(R"(-?[[:digit:]]+\.[[:digit:]]+)");
		std::regex const pattern_int(R"(-?[[:digit:]]+)");
		if (operand < Float) {
			if (std::regex_match(str, pattern_float)){
				throw(CustomException("[parsing][value][int] - token '" + str + "' is not a valid INT value"));
			}
			else if (!std::regex_match(str, pattern_int)){
				throw(CustomException("[parsing][value][int] - token '" + str + "' is not a valid value"));
			}
		} 
		else {
			if (!(std::regex_match(str, pattern_float) || std::regex_match(str, pattern_int))){
				throw(CustomException("[parsing][value][float] - token '" + str + "' is not a valid value"));
			}
		}
	}
}

void	Instruction::parseInstruction(void){
	if (lexedText[INSTRUCTION].empty() == false){
		instruction = parseInstruction(lexedText[INSTRUCTION]);
	}
}

eOperandType		Instruction::parseOperand(std::string str) const {
	std::string ustr = str;
	std::transform(ustr.begin(), ustr.end(), ustr.begin(), ::tolower);

	if (ustr.compare("int8") == 0) 		return Int8;
	if (ustr.compare("int16") == 0) 	return Int16;
	if (ustr.compare("int32") == 0) 	return Int32;
	if (ustr.compare("float") == 0) 	return Float;
	if (ustr.compare("double") == 0) 	return Double;
	throw (CustomException("[parsing][operand] - token '" + str + "' is not a valid operand."));
}

eInstructionType	Instruction::parseInstruction(std::string str) const {
	std::string ustr = str;
	std::transform(ustr.begin(), ustr.end(), ustr.begin(), ::tolower);
	if (ustr.compare("push") == 0) 		return PUSH;
	if (ustr.compare("pop") == 0) 		return POP;
	if (ustr.compare("dump") == 0) 		return DUMP;
	if (ustr.compare("assert") == 0) 	return ASSERT;
	if (ustr.compare("add") == 0) 		return ADD;
	if (ustr.compare("sub") == 0) 		return SUB;
	if (ustr.compare("mul") == 0) 		return MUL;
	if (ustr.compare("div") == 0) 		return DIV;
	if (ustr.compare("mod") == 0) 		return MOD;
	if (ustr.compare("print") == 0) 	return PRINT;
	if (ustr.compare("exit") == 0) 		return EXIT;
	throw (CustomException("[parsing][instruction] - token '" + str + "' is not a valid instruction."));
}