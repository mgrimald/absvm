#include <iostream>
#include "AbstractVm.hpp"
#include "Factory.hpp"


int Factory::test(void){
	int errors = 0;
	std::string verif;
	std::string cpr;
	std::string test;

	try {
		errors += 1;
		createNone("abc");
		std::cout << "createNone should have throw an error" << std::endl;
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		errors += 1;
		createInt8("ABC");
		std::cout << "createInt8 with invalid argument should have throw an error" << std::endl;
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		errors += 1;
		createInt8("-599");
		std::cout << "createInt8 with an underflow should have throw an error" << std::endl;
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		errors += 1;
		createInt8("599");
		std::cout << "createInt8 with an overflow should have throw an error" << std::endl;
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		errors += 1;
		createFloat("500000000000000000000000000000000000000000000000515614805132481524512124521412541021555422452412451245415241524544141541245542541245154544155450.99");
		std::cout << "createFloat with an overflow should have throw an error" << std::endl;
	}
	catch (const CustomException &e){
		//std::cout << e.what() << std::endl;
		errors -=1;
	}
	try {
		errors += 1;
		createFloat("-500000000000000000000000000000000000000000000000515614805132481524512124521412541021555422452412451245415241524544141541245542541245154544155450.99");
		std::cout << "createFloat with an underflow should have throw an error" << std::endl;
	}
	catch (const CustomException &e){
		//std::cout << e.what() << std::endl;
		errors -=1;
	}
	try {
		cpr = "-500000000000474147414174141784512045120784512078451207812045120451200000000000000000000000000000000000000000000000515614805132481524512124521412541021555422452412451245415241524544141541245542541245154544155450.990000";
		const IOperand *t = createDouble(cpr);
		test = t->toString();
	}
	catch (const CustomException &e){
		errors += 1;
		std::cout << "createDouble errors" << std::endl;
	}
	try {
		cpr = "-500000000000474147414174141784512045120784512078451207812045120451200000000000000000000000000000000000000000000000515614805132481524512124521412541021555422452412451245415241524544141541245542541245154544155450.99";
		const IOperand *t = createOperand(Double, cpr);
		test = t->toString();
	}
	catch (const CustomException &e){
		errors += 1;
		std::cout << "createDouble errors" << std::endl;
	}
	try {
		errors +=1;
		cpr = "-500000000000474147414174141784512045120784512078451207812045120451200000000000000000000000000000000000000000000000515614805132481524512124521412541021555422452412451245415241524544141541245542541245154544155450.990000";
		const IOperand *t = createOperand(Float, cpr);
		test = t->toString();
		std::cerr << "ERROR\n\t(Factory/IOperand) createOperand(Float, cpr) toString returned (" << test << ") rather than (" << cpr << ")" << std::endl;
	}
	catch (const CustomException &e){
		errors -= 1;
	}
	try {
		cpr = "-50.99";
		verif = "-50";
		const IOperand *t = createOperand(Int32, cpr);
		test = t->toString();
		if (test.compare(verif) != 0 ){
			errors += 1;
			std::cerr << "ERROR\n\t(Factory/IOperand) createOperand(Int32, cpr) toString returned (" << test << ") rather than (" << verif << ")" << std::endl;
		}
	}
	catch (const CustomException &e){
		errors += 1;
	}
	try {
		cpr = "50.99";
		verif = "50";
		const IOperand *t = createOperand(Int32, cpr);
		test = t->toString();
		if (test.compare(verif) != 0 ){
			errors += 1;
			std::cerr << "ERROR\n\t(Factory/IOperand) createOperand(Int32, cpr) toString returned (" << test << ") rather than (" << verif << ")" << std::endl;
		}
	}
	catch (const CustomException &e){
		errors += 1;
	}

	try {
		cpr = "-0";
		verif = "0";
		const IOperand *t = createOperand(Int32, cpr);
		test = t->toString();
		if (test.compare(verif) != 0 ){
			errors += 1;
			std::cerr << "ERROR\n\t(Factory/IOperand) createOperand(Int32, cpr) toString returned (" << test << ") rather than (" << verif << ")" << std::endl;
		}
	}
	catch (const CustomException &e){
		errors += 1;
		std::cout << "createInt32 errors" << std::endl;
	}
	return errors;
}


int Instruction::test(void){
	int errors = 0;

	eInstructionType cpr;
	std::string scpr;
	eInstructionType test;

	scpr = "PUSH";
	cpr = PUSH;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "POP";
	cpr = POP;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "DUMP";
	cpr = DUMP;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "ASSERT";
	cpr = ASSERT;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "ADD";
	cpr = ADD;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "SUB";
	cpr = SUB;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "MUL";
	cpr = MUL;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "DIV";
	cpr = DIV;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "MOD";
	cpr = MOD;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "PRINT";
	cpr = PRINT;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "Print";
	cpr = PRINT;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "print";
	cpr = PRINT;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "EXIT";
	cpr = EXIT;
	test = parseInstruction(scpr);
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseInstruction() returned (" << test << ") rather than (" << cpr << ") from string : '" << scpr << "'" << std::endl;
	}

	try {
		scpr = "";
		errors += 1;
		test = parseInstruction(scpr);
		std::cout << "Instruction parseInstruction() returned (" << test <<  ") rather than throwing an exception for '" << scpr << "'" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		scpr = "EXITTTTTT";
		errors += 1;
		test = parseInstruction(scpr);
		std::cout << "Instruction parseInstruction() returned (" << test <<  ") rather than throwing an exception for '" << scpr << "'" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		scpr = "wazsedxcfjnkml,oinubrewzEXITTTTTT";
		errors += 1;
		test = parseInstruction(scpr);
		std::cout << "Instruction parseInstruction() returned (" << test <<  ") rather than throwing an exception for '" << scpr << "'" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		scpr = "  DUMP";
		errors += 1;
		test = parseInstruction(scpr);
		std::cout << "Instruction parseInstruction() returned (" << test <<  ") rather than throwing an exception for '" << scpr << "'" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}


	eOperandType cp;
	eOperandType tes;

	scpr = "int8";
	cp = Int8;
	tes = parseOperand(scpr);
	if (tes != cp){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseOperand() returned (" << tes << ") rather than (" << cp << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "int16";
	cp = Int16;
	tes = parseOperand(scpr);
	if (tes != cp){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseOperand() returned (" << tes << ") rather than (" << cp << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "int32";
	cp = Int32;
	tes = parseOperand(scpr);
	if (tes != cp){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseOperand() returned (" << tes << ") rather than (" << cp << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "float";
	cp = Float;
	tes = parseOperand(scpr);
	if (tes != cp){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseOperand() returned (" << tes << ") rather than (" << cp << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "FlOat";
	cp = Float;
	tes = parseOperand(scpr);
	if (tes != cp){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseOperand() returned (" << tes << ") rather than (" << cp << ") from string : '" << scpr << "'" << std::endl;
	}

	scpr = "DOUBLE";
	cp = Double;
	tes = parseOperand(scpr);
	if (tes != cp){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction parseOperand() returned (" << tes << ") rather than (" << cp << ") from string : '" << scpr << "'" << std::endl;
	}

	try {
		scpr = "";
		errors += 1;
		tes = parseOperand(scpr);
		std::cout << "Instruction parseOperand() returned (" << tes <<  ") rather than throwing an exception for '" << scpr << "'" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}

	try {
		scpr = "None";
		errors += 1;
		tes = parseOperand(scpr);
		std::cout << "Instruction parseOperand() returned (" << tes <<  ") rather than throwing an exception for '" << scpr << "'" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}

	return errors;
}

int AbstractVm::test(void){
	int errors = 0;
	std::string fileToRead;
	/*			*/
	try {
		fileToRead = "/erxcftgvbhjnk/fsad";
		errors += 1;
		readInputs(fileToRead);
		std::cout << "file '" << fileToRead << "' should have failed to open (doesn't exist)" << std::endl; 
	}
	catch (const CustomException &e){
		errors -=1;
	}
	try {
		fileToRead = "/sgoinfre/goinfre/Perso/nsehnoun/._League of Legends.app";
		errors += 1;
		readInputs(fileToRead);
		std::cout << "file '" << fileToRead << "' should have failed to open (doesn't have the right to open)" << std::endl;
	}
	catch (const CustomException &e){
		errors -=1;
	}
	/*			*/
	try {
		fileToRead = "inputsFiles/invalidDumpAndExit";
		errors += 1;
		readInputs(fileToRead);
		std::cout << "file '" << fileToRead << "' should have throw an error to handle a parse error (final line)" << std::endl;
	}
	catch (const CustomException &e){
		errors -=1;
	}
	try {
		fileToRead = "inputsFiles/dumpAndExit";
		std::queue<Instruction> ret = readInputs(fileToRead);
		if (readInputs(fileToRead).empty() == true) {
			errors +=1;
			std::cout << "file '" << fileToRead << "' should have returned an empty queue" << std::endl;
		}
		if (ret.front().getText().compare("dump") != 0){
			std::cout << "first element of the queue from file '" << fileToRead << "' should be equal to 'dump'. Instead it's value is : " << ret.front().getText() << std::endl;
			return errors + 1;
		}
		std::array<std::string, 3> arr = ret.front().getLexedText();
		if (arr[INSTRUCTION].compare("dump") != 0 || ret.front().getInstruction() != DUMP ){
			std::cout << "INSTRUCTION first element of the queue from file '" << fileToRead << "' should be equal to 'dump'. Instead it's value is : '" << arr[INSTRUCTION] << "' and getInstruction returned " << ret.front().getInstruction() << std::endl;			
			return errors + 1;
		}
		if (arr[OPERAND].compare("") != 0 || ret.front().getOperand() != None ){
			std::cout << "INSTRUCTION first element of the queue from file '" << fileToRead << "' should be equal to 'dump'. Instead it's value is : '" << arr[OPERAND] << "' and getOperand returned " << ret.front().getOperand() << std::endl;			
			return errors + 1;
		}
		if (arr[VALUE].compare("") != 0){
			std::cout << "VALUE first element of the queue from file '" << fileToRead << "' should be empty. Instead it's value is : '" << arr[VALUE] << std::endl;			
			return errors + 1;
		}
	}
	catch (const CustomException &e){
		errors +=1;
		std::cout << "file '" << fileToRead << "' should have succeded to open.\n\te.what() : " << e.what() << std::endl;
	}
	try {
		fileToRead = "inputsFiles/empty";
		std::queue<Instruction> ret = readInputs(fileToRead);
		if (ret.empty() == false) {
			errors += 1;
			std::cout << "file '" << fileToRead << "' shouldn't have returned an empty queue" << std::endl;
			return errors;
		}
	}
	catch (const CustomException &e){
		errors +=1;
		std::cout << "file '" << fileToRead << "' should have succeded to open" << std::endl;
	}
	return errors;
}

static int testInstructionOperand(void){
	int errors = 0;
	eOperandType cpr;
	eOperandType test;
	Instruction *instruct = new Instruction();

	cpr = None;
	test = instruct->getOperand();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getOperand() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cpr = None;
	Instruction another = Instruction("<<constructor with string test>>");
	test = another.getOperand();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getOperand() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cpr = Int8;
	instruct->setOperand(cpr);
	test = instruct->getOperand();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getOperand() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	instruct->setOperand(Int32);
	cpr = instruct->getOperand();
	another = Instruction(*instruct);
	test = another.getOperand();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getOperand() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	delete (instruct);
	return errors;
}

static int testInstructionInstruction(void){
	int errors = 0;
	eInstructionType cpr;
	eInstructionType test;
	Instruction *instruct = new Instruction();

	cpr = NONE;
	test = instruct->getInstruction();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getInstruction() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cpr = NONE;
	Instruction another = Instruction("<<constructor with string test>>");
	test = another.getInstruction();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getInstruction() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cpr = POP;
	instruct->setInstruction(cpr);
	test = instruct->getInstruction();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getInstruction() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	instruct->setInstruction(SUB);
	cpr = instruct->getInstruction();
	another = Instruction(*instruct);
	test = another.getInstruction();
	if (test != cpr){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getInstruction() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	delete (instruct);
	return errors;
}


static int testInstructionText(void){
	int errors = 0;
	std::string cpr;
	std::string test;
	Instruction *instruct = new Instruction();

	cpr = "<Empty Instruction>";

	test = instruct->getText();
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getText() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cpr = "<<setText test>>";
	instruct->setText(cpr);
	test = instruct->getText();
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getText() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cpr = "<<constructor with string test>>";
	Instruction another = Instruction(cpr);
	test = another.getText();
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getText() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	instruct->setText("<<copy instruct test>>");
	cpr = instruct->getText();
	another = *instruct;
	test = another.getText();
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getText() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	delete (instruct);
	return errors;
}

static int testInstructionLexedText(void){
	int errors = 0;
	std::string cpr;
	std::string test;
	Instruction *instruct = new Instruction();
	std::array<std::string, 3> testArray3;
	std::array<std::string, 3> cprArray3;

	cpr = "";
	test = instruct->getLexedText(INSTRUCTION);
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText(INSTRUCTION) returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	cpr = "<<setLexedText INSTRUCTION test>>";
	instruct->setLexedText(cpr, INSTRUCTION);
	test = instruct->getLexedText(INSTRUCTION);
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText(INSTRUCTION) returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	
	cpr = "";
	test = instruct->getLexedText(OPERAND);
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText(OPERAND) returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	cpr = "<<setLexedText OPERAND test>>";
	instruct->setLexedText(cpr, OPERAND);
	test = instruct->getLexedText(OPERAND);
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText(OPERAND) returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	
	cpr = "";
	test = instruct->getLexedText(VALUE);
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText(VALUE) returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}
	cpr = "<<setLexedText VALUE test>>";
	instruct->setLexedText(cpr, VALUE);
	test = instruct->getLexedText(VALUE);
	if (test.compare(cpr) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText(VALUE) returned '" << test << "' rather than '" << cpr << "'" << std::endl;
	}

	cprArray3[0] = "";
	cprArray3[1] = "";
	cprArray3[2] = "";
	Instruction *t = new Instruction();
	testArray3 = t->getLexedText();
	if (testArray3[0].compare(cprArray3[0]) != 0 || testArray3[1].compare(cprArray3[1]) != 0 || testArray3[2].compare(cprArray3[2]) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText() returned '" << testArray3[0] << "' rather than '" << cprArray3[0] << "'" << std::endl;
		std::cerr << "\tInstruction getLexedText() returned '" << testArray3[1] << "' rather than '" << cprArray3[1] << "'" << std::endl;
		std::cerr << "\tInstruction getLexedText() returned '" << testArray3[2] << "' rather than '" << cprArray3[2] << "'" << std::endl;
	}

	cprArray3[0] = "<<setLexedText 0 test>>";
	cprArray3[1] = "<<setLexedText 1 test>>";
	cprArray3[2] = "<<setLexedText 2 test>>";
	t->setLexedText(cprArray3);
	testArray3 = t->getLexedText();
	if (testArray3[0].compare(cprArray3[0]) != 0 || testArray3[1].compare(cprArray3[1]) != 0 || testArray3[2].compare(cprArray3[2]) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText() returned '" << testArray3[0] << "' rather than '" << cprArray3[0] << "'" << std::endl;
		std::cerr << "\tInstruction getLexedText() returned '" << testArray3[1] << "' rather than '" << cprArray3[1] << "'" << std::endl;
		std::cerr << "\tInstruction getLexedText() returned '" << testArray3[2] << "' rather than '" << cprArray3[2] << "'" << std::endl;
	}

	cprArray3[0] = "<<setLexedText copy constructor 0 test>>";
	cprArray3[1] = "<<setLexedText copy constructor 1 test>>";
	cprArray3[2] = "<<setLexedText copy constructor 2 test>>";
	t->setLexedText(cprArray3);
	delete(instruct);
	instruct = new Instruction(*t);
	testArray3 = instruct->getLexedText();
	if (testArray3[0].compare(cprArray3[0]) != 0 || testArray3[1].compare(cprArray3[1]) != 0 || testArray3[2].compare(cprArray3[2]) != 0){
		errors += 1;
		std::cerr << "ERROR\n\tInstruction getLexedText() returned '" << testArray3[0] << "' rather than '" << cprArray3[0] << "'" << std::endl;
		std::cerr << "\tInstruction getLexedText() returned '" << testArray3[1] << "' rather than '" << cprArray3[1] << "'" << std::endl;
		std::cerr << "\tInstruction getLexedText() returned '" << testArray3[2] << "' rather than '" << cprArray3[2] << "'" << std::endl;
	}
	delete(t);
	delete(instruct);
	return errors;
}

static int testInstruction(void){
	int errors = 0;
	std::string cpr;

	Instruction *instruct = new Instruction();
	errors += instruct->test();

	errors += testInstructionText();
	errors += testInstructionOperand();
	errors += testInstructionInstruction();
	errors += testInstructionLexedText();

	delete(instruct);
	return errors;
}

static int testExceptions(void){
	int errors = 0;
	std::string cpr;
	try {
		throw(SilentException());
	}
	catch (const CustomException& e){
		std::string test = e.what();
		cpr = "(chuuuuut, this is a silent exception, used to go back to the upper frame)";

		if (test.compare(cpr) != 0){
			errors += 1;
			std::cerr << "ERROR\n\tSilentException e.what() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
		}
	}
	try {
		throw(CustomException());
	}
	catch (const CustomException& e){
		std::string test = e.what();
		cpr = "<default message for CustomException>";

		if (test.compare(cpr) != 0){
			errors += 1;
			std::cerr << "ERROR\n\tCustomException e.what() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
		}
	}
	cpr = "abc";
	try {
		throw(CustomException(cpr));
	}
	catch (const CustomException& e){
		std::string test = e.what();

		if (test.compare(cpr) != 0){
			errors += 1;
			std::cerr << "ERROR\n\tCustomException e.what() returned '" << test << "' rather than '" << cpr << "'" << std::endl;
		}
	}
	return errors;
}

static int testManager(void){
	int errors = 0;
	int prevErrors;

	prevErrors = errors;
	{
		AbstractVm *aVm = new AbstractVm();
		errors += aVm->test();
		delete aVm;
	}
	if (prevErrors != errors){
		std::cerr << "errors occured in AbstractVm tests" << std::endl;
	}
	else
		std::cout << "AbstractVm : OK" << std::endl;

	prevErrors = errors;
	{
		errors += testExceptions();
	}
	if (prevErrors != errors){
		std::cerr << "errors occured in Exceptions tests" << std::endl;
	}
	else
		std::cout << "AbstractVm : OK" << std::endl;

	prevErrors = errors;
	{
		errors += testInstruction();
	}
	if (prevErrors != errors){
		std::cerr << "errors occured in Instruction tests" << std::endl;
	}
	else
		std::cout << "Instruction : OK" << std::endl;

	prevErrors = errors;
	{
		errors += Factory::getInstance()->test();
		Factory::kill();
	}
	if (prevErrors != errors){
		std::cerr << "errors occured in Factory tests" << std::endl;
	}
	else
		std::cout << "Factory : OK" << std::endl;


	return errors;
}

int test(void){
	int testResult = testManager();
	if (testResult == 0){
		std::cerr << "ALL TEST CLEARED\n:)" << std::endl;
		return 0;
	}
	else {
		std::cout << "\nERRORS OCCURED\nERRORS OCCURED\nERRORS OCCURED\n";
		std::cerr << "\ttest:\t" << testResult << std::endl;
		std::cout << "ERRORS OCCURED\nERRORS OCCURED\nERRORS OCCURED" << std::endl;
		return testResult;
	}
}